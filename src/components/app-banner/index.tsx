import React from "react";
import {
  Typography,
  Avatar,
  Card,
  CardContent,
  CardMedia,
  Grid,
} from "@mui/material";
// @ts-ignore
import classNames from "classnames";
import useStyles from "./app-banner.styles";

export default function AppBanner() {
  const classes = useStyles();

  return (
    <Grid
      container
      className={classes.container}
      justifyContent="space-around"
    >
        <Typography variant="h2"
          component="h2"
          gutterBottom
          className={classes.title}
          >
            Built on top of
        </Typography>
      <Grid
        item
        xs={12}
        sm={12}
        justifyContent="center"
        alignContent="center"
        className={classes.content}
      >
        <div className={classes.banner}>
          <img src={require('../../assets/logos/mongodb-ar21.svg').default.src}
            title="MongoDB official logo"
            alt="MongoDB official logo"
            className={classes.media} />
          <img src={require('../../assets/logos/amazon-web-services.svg').default.src}
            title="AWS official logo"
            alt="AWS official logo"
            className={classes.media} />
          <img src={require('../../assets/logos/nestjs-icon.svg').default.src}
            title="NestJS official logo"
            alt="NestJS official logo"
            className={classes.media} />
          <img src={require('../../assets/logos/nodejs.svg').default.src}
            title="NodeJS official logo"
            alt="NodeJS official logo"
            className={classes.media} />
          <img src={require('../../assets/logos/google_cloud-ar21.svg').default.src}
            title="GCP official logo"
            alt="GCP official logo"
            className={classes.media} />
          <img src={require('../../assets/logos/postgresql-vertical.svg').default.src}
            title="NodeJS official logo"
            alt="NodeJS official logo"
            className={classes.media} />
        </div>
      </Grid>
    </Grid>
  );
}
