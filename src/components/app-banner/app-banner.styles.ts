import makeStyles from '@mui/styles/makeStyles';

export default makeStyles({
  container: {
    margin: "0",
    width: "100%",
    backgroundColor: "#FFF"
  },
  content: {
    display: "flex",
    flexDirection: "row",
    padding: "0px !important",
    width: "100%",
    maxWidth: "100%",
    justifyContent: "space-between",
  },
  media: {
    maxHeight: 60,
  },
  title: {
    textTransform: "uppercase",
    marginTop: "25px",
    fontSize: "2.5em",
    textAlign: "center",
    color: "#2B2E31",
  },
  banner: {
    maxWidth: "1200px",
    width: "100%",
    margin: "0 auto",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  }
});