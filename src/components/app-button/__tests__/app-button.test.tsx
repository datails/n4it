import React from "react";
import { render, fireEvent } from "@testing-library/react";
import CustomizedButtons from "../index";

test("button click handler is called", () => {
  const mockHandler = jest.fn();
  const { getByText } = render(
    <CustomizedButtons handler={mockHandler}>Click me</CustomizedButtons>
  );
  const button = getByText("Click me");
  fireEvent.click(button);
  expect(mockHandler).toHaveBeenCalled();
});
