import React from "react";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import Button from "@mui/material/Button";

const useStyles = makeStyles((theme: Record<string, any>) =>
  createStyles({
    button: {
      color: "#FFF",
      fontSize: "1.1rem",
      height: "50px",
      minWidth: "175px",
      padding: "0 30px",
      margin: 0,
      borderRadius: "4px",
      backgroundColor: theme.palette.primary.main,
      "&:hover": {
        backgroundColor: theme.palette.primary.dark,
      },
    },
  })
);

type Props = {
  props?: any;
  children: React.ReactNode;
  styles?: React.CSSProperties;
  href?: string;
  disabled?: boolean;
  handler?: React.MouseEventHandler<HTMLAnchorElement>;
};

export default function CustomizedButtons({
  props,
  children,
  styles,
  href,
  disabled = false,
  handler,
}: Props) {
  const classes = useStyles();

  return (
    <Button
      disabled={disabled}
      onClick={handler}
      href={href}
      variant="contained"
      // color="primary"
      style={styles || {}}
      className={classes.button}
      {...props}
    >
      {children}
    </Button>
  );
}
