import React from "react";
import { Grid, Typography } from "@mui/material";
import useStyles from "./app-hero-header.styles";

type Props = {
  subTitle: string;
  title: string;
  background: string;
  local?: boolean;
};

export default function AppHeroHeader({
  subTitle,
  title,
  background,
  local = true,
}: Props) {
  const classes = useStyles();

  const injectBackGroundImage = (image: string) => ({
    backgroundImage: `url(${image})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundAttachment: "fixed",
  });

  return (
    <React.Fragment>
      <Grid
        data-testid="hero-container"
        container
        spacing={10}
        className={classes.container}
        justifyContent="space-around"
        // style={injectBackGroundImage(
        //   local
        //     ? require(`../../assets/${background.toLowerCase()}`).default.src
        //     : background
        // )}
      >
        <Grid item xs={12} className={classes.content}>
          <Typography
            component="h1"
            variant="h2"
            align="center"
            className={classes.title}
            gutterBottom
          >
            {title}
          </Typography>
          {subTitle && (
            <Typography
              variant="h5"
              align="center"
              className={classes.subTitle}
              paragraph
            >
              {subTitle}
            </Typography>
          )}
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
