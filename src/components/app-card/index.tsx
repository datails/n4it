import React from "react";
import {
  Card,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
//@ts-ignore
import classnames from "classnames";

import useStyles from "./app-card.styles";

type Props = {
  desc: string;
  image: string;
  title: string;
};

export default function AppCard({
  desc,
  image,
  title,
}: Props) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardMedia
        component="img"
        alt={title}
        height="140"
        image={image}
        title={title}
        className={classes.media}
      />
      <CardContent
        className={classnames(classes.content, classes.padding)}
      >
        <Typography
          gutterBottom
          variant="h5"
          component="h2"
          className={classes.title}
        >
          {title}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {desc}
        </Typography>
      </CardContent>
    </Card>
  );
}
