import makeStyles from '@mui/styles/makeStyles';

export default makeStyles({
    container: {
      margin: "0",
      width: "100%",
    },
    content: {
      display: "flex",
      flexDirection: "column",
      padding: "0px !important",
      width: "100%",
      justifyContent: "center",
    },
    card: {
      width: "100%",
      boxShadow: "0 0 2.5rem 0.3125rem rgba(0,0,0,.1)",
      color: "#2a2c2f",
    },
    media: {
      height: 200,
    },
    textDecorationNone: {
      textDecoration: "none!important",
    },
    padding: {
      padding: "25px 15px !important",
    },
    cardActions: {
      display: "flex",
      justify: "space-around",
    },
    title: {
      fontWeight: 500,
      textAlign: "center",
    },
  });