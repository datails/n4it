import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export default makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0 auto",
      width: "100%",
      maxWidth: "1200px",
      padding: "40px",
      display: "flex",
      alignContent: "center",
      alignItems: "center",
      background: "#FFF",
      [theme.breakpoints?.down("md")]: {
        padding: "40px 0",
      },
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      padding: "40px",
    },
    image: {
      maxHeight: "300px",
      width: "100%",
      borderRadius: "0.25rem",
      margin: "0 auto"
    },
    text: {
      fontFamily:`monospace`,
      fontWeight: 500,
      lineHeight: 1.8,
      textAlign: "justify",
      fontSize: "1.1rem",
    },
    title: {
      display: "flex",
      textTransform: "uppercase",
      fontFamily: "monospace",
      fontWeight: 700,
      fontSize: "2rem",
      position: "relative",
      marginBottom: "2rem",
      lineHeight: 2,
      "&::after": {
        position: "absolute",
        content: '""',
        bottom: 0,
        width: "80px",
        height: "3px",
        background: theme.palette.secondary.dark,
        transform: "translateX(0%)",
        [theme.breakpoints?.down("md")]: {
          display: "none",
        },
      },
    },
  })
);
