import React from "react";
import { Grid, Typography } from "@mui/material";
import ReactMarkdown from "react-markdown";
import useStyles from "./app-text-image.styles";

type Props = {
  text: string;
  title: string;
  reverse?: boolean;
  image?: string;
  backgroundColor?: string;
  children?: any;
};

export default function AppTextImage({
  text,
  title,
  reverse = false,
  image,
  backgroundColor,
  children,
}: Props) {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid
        container
        justifyContent="space-around"
        direction={reverse ? "row-reverse" : "row"}
        className={classes.container}
        spacing={10}
        style={{ backgroundColor: backgroundColor }}
      >
        <Grid item xs={12} sm={7} className={classes.content}>
          {title ? (
            <Typography variant="h2" gutterBottom className={classes.title}>
              {title}
            </Typography>
          ) : (
            ""
          )}
          <ReactMarkdown className={classes.text}>{text}</ReactMarkdown>
          {children}
        </Grid>
        {image && (
          <Grid item xs={12} sm={5} className={classes.content}>
            <img
              src={image}
              alt={title}
              className={classes.image}
            />
          </Grid>
        )}
      </Grid>
    </React.Fragment>
  );
}
