import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export default makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0",
      width: "100%",
      padding: "160px 40px 160px",
      background: "#FFF",
      [theme.breakpoints?.down("md")]: {
        padding: "40px 0",
      },
    },
    content: {
      fontWeight: 400
    },
    title: {
      textTransform: "uppercase",
      marginBottom: "50px",
      fontSize: "2.5em",
    },
  })
);
