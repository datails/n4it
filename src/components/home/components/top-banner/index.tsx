import React from "react";
import { Grid, Typography } from "@mui/material";
import useStyles from "./top-banner.styles";

type Props = {
  title?: string;
  backgroundColor?: string;
};

export default function ComponentTopBanner({ title, backgroundColor }: Props) {
  const classes = useStyles();

  return (
    <Grid
      container
      className={classes.container}
      justifyContent="space-evenly"
      spacing={10}
      style={{ backgroundColor: backgroundColor }}
    >
      <Grid item lg={8} xl={6} md={12} justifyContent="center" textAlign={"center"}>
          <Grid sm={12} justifyContent="center" textAlign={"center"}>
            <Typography
              variant="h2"
              component="h2"
              gutterBottom
              className={classes.title}
            >
            Don&apos;t reinvent the wheel. Use our<br/>
            NodeJS Modules
            </Typography>
            <Typography variant="subtitle1" component="h3" className={classes.content}>
              Implementing a backend stack yourself takes time and effort. There are many choices to be made in open source solutions and integrating them is time-consuming. Building a scalable and secure platform requires expertise. With our code libraries we can implement and integrate a complete backend stack with authentication, database, CRUD, logging, security best-practices and more. Production grade and ready to go within a week.
            </Typography>
          </Grid>
        </Grid>
    </Grid>
  );
}
