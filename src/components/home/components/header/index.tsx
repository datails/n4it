import React from "react";
import { Grid, Typography } from "@mui/material";
import { GitHub } from "@mui/icons-material";
import useStyles from "./header.styles";
import { AppButton } from "@/components";

export default function Header() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid container spacing={10} className={classes.container} justifyContent="center">
        <Grid item xs={12} sm={6} md={6} className={classes.content}>
          <Typography
            variant="h1"
            component="h1"
            gutterBottom
            className={classes.title}
          >
            NestJS.<br/> solution design.<br/> Infrastructure (IaC)<br/> Development.<br/> Opensource.
          </Typography>
          <Typography
            variant="h2"
            component="h2"
            gutterBottom
            className={classes.subTitle}
          >
            Stay tuned for more...
          </Typography>
          {/* <Typography className={classes.text}>
            Over the years, we've helped over 10 different companies develop NestJS applications. These companies varied in size, from enterprises to scale-ups, and from scale-ups to startups. But they all had one thing in common: NodeJS, and especially NestJS, was used as a BFF (Backend-for-Frontend) layer. However, the challenges these companies faced were strikingly similar. Interestingly enough, they all reinvented the wheel—choosing to build rather than buy, driven by engineers focused on custom solutions instead of relying on open-source tools to handle repetitive tasks. This would have allowed them to focus on adding critical business value.<br/><br/>
            The problem of reinventing the wheel for most companies often began with authentication. Implementing a full OIDC solution using a cloud service seemed really complicated for developers, even though it’s a common task for most companies. But it's also extremely critical. This repeated struggle triggered our frustration, especially as many companies implemented it incorrectly. They kept focusing on these no-brainers instead of utilizing full-blown open-source solutions. From this frustration, N4IT was born—or Nest 4 It, if you prefer. We are a consulting and development company that guides companies in developing NestJS applications quickly. More importantly, we help them build robust, maintainable applications using battle-tested solutions we've developed across over 25 NestJS projects.<br/>

            And the best part? We will open-source it all. So, what's our added value? Simple—most companies struggle with these solutions, especially in the NodeJS environment. Plus, NestJS is just a tool; you also need Terraform. We've developed a broad range of Terraform Modules that seamlessly integrate with a variety of open-source NestJS packages.

            Stay tuned, and keep an eye on our GitHub.
          </Typography>  */}
          <AppButton styles={{
            backgroundColor: "#2B2E31",
            justifyContent: "space-between",
            display: "flex",
            // @ts-ignore
            "&:hover": { 
              backgroundColor: "#2B2B30",
            },
          }} href="https://github.com/orgs/nest4it/repositories">
            <span style={{ marginRight: "25px"}}>Repositories</span> <GitHub />
          </AppButton>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
