import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export default makeStyles((theme) =>
  createStyles({
    colorWhite: {
      color: "#fff",
    },
    container: {
       minHeight: "80vh",
        alignItems: "center",
        margin: 0,
        width: "100%",
        position: "relative",
        overflow: "hidden",
        background: "linear-gradient(90deg, #ea2862, #151515)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
          [theme.breakpoints?.down("md")]: {
            minHeight: "50vh",
            backgroundAttachment: "inherit",
          },
    },
    content: {
      position: "relative",
      display: "flex",
      alignItems: "center",
      flexDirection: "column",
      [theme.breakpoints?.down("md")]: {
        marginLeft: "0px",
        maxWidth: "100%",
        textAlign: "center",
      },
    },
    title: {
      color: "#FFF",
      fontWeight: 900,
      textAlign: "center",
      textTransform: "uppercase",
      fontSize: "3rem",
      // background: "rgba(0,0,0,0.07)",
      padding: "25px",
    },
    text: {
      color: "#FFF",
      textAlign: "center",
      padding: "25px",
      background: "rgba(0,0,0,0.07)",
    },
    subTitle: {
      paddingBottom: "25px",
      color: "#FFF",
      textAlign: "center",
      fontSize: "22px",
      fontWeight: 500,
      // background: "rgba(0,0,0,0.07)",
      [theme.breakpoints?.down("md")]: {
        display: "none",
      },
    },
  })
);
