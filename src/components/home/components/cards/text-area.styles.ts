import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export default makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0 auto",
      width: "100%",
      padding: "160px 40px 0",
      background: "#2b2e31",
      color: "#FFF",
      // @ts-ignore
      [theme.breakpoints?.down("md")]: {
        padding: "40px 0",
      },
    },
    backgroundMain: {
      background: "#2b2e31",
      color: "#FFF",
    },
    secondContainer: {
      margin: "0 auto",
      width: "100%",
      maxWidth: "1200px",
      padding: "0px 40px 160px",
      background: "#2b2e31",
      color: "#FFF",
      // @ts-ignore
      [theme.breakpoints?.down("md")]: {
        padding: "40px 0",
      },
    },
    content: {
      fontWeight: 400
    },
    title: {
      marginBottom: "50px",
      fontSize: "2.5em",
      textTransform: "uppercase",
      color: "#FFF",
    },
  })
);
