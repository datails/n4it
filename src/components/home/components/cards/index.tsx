import React from "react";
import { AppCard } from "../../..";
import { Grid, Typography } from "@mui/material";
import useStyles from "./text-area.styles";

function Cards() {
  const classes = useStyles();

  return (
    <>
      <Grid
        container
        className={classes.container}
        justifyContent="space-evenly"
        spacing={10}
      >
      <Grid item xl={6} md={12} justifyContent="center" textAlign={"center"}>
        <Typography variant="h2" gutterBottom className={classes.title}>
          You decide. We provide.
        </Typography>
      </Grid>
      </Grid>
      <div className={classes.backgroundMain}>
        <Grid
          container
          className={classes.secondContainer}
          justifyContent="space-evenly"
          spacing={10}
        >
          <Grid item sm={6} xs={12} justifyContent="center" textAlign={"center"}>
            <AppCard title="Headless" desc="With support for both REST, GraphQL and gRPC, you can use our modules for any frontend (or backend) technology. Our Dynamic Modules seamlessly integrates with your data models." image={require("../../../../assets/logos/graphql_rest_grpc.webp").default.src} />
          </Grid>
          <Grid item sm={6} xs={12} justifyContent="center" textAlign={"center"}>
            <AppCard desc={`
              No vendor lock-in. You can use our modules in any cloud provider. With SDK's for NodeJS and GoLang, and integrations with AWS and GCP resources, you chose we deliver.
            `} title="Plug-and-play" image={require("../../../../assets/guidance.jpeg").default.src} />
          </Grid>
        </Grid> 
      </div>
    </>

  );
}

export default Cards;
