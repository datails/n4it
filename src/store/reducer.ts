export const reducer = (state: any, action: any) => {
  let newState = {};

  switch (action.type) {
    case "TOGGLE_DRAWER":
      newState = {
        ...state,
        drawer: !state.drawer,
      };
      break;

    case "TOGGLE_LOADER":
      newState = {
        ...state,
        loader: action.status || !state.loader,
      };
      break;
    default:
      newState = state;
  }

  localStorage.setItem("root:state", JSON.stringify(newState));

  return newState;
};

export default reducer;