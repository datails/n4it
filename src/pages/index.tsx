import React from "react";
import Head from "next/head";
import { Divider } from "@mui/material";

import AppHeader from "../components/home/components/header";
import AppTextImage from "@/components/app-text-image";
import Cards from "@/components/home/components/cards";
import { GetStaticProps } from "next";
import AppBanner from "@/components/app-banner";

type Props = {
  data: {
    title: string;
    description: string;
    url: string;
  };
};

function Home({ data }: Props) {
  return (
    <React.Fragment>
      <Head>
        <title>{data.title}</title>
        <meta
          name="description"
          content={data.description}
        />
        <meta
          property="og:description"
          content={data.description}
        />
        <meta property="og:title" content={data.title} />
        <meta
          property="og:url"
          content={data.url}
        />
      </Head>
      <AppHeader />
    </React.Fragment>
  );
}

export const getStaticProps: GetStaticProps<Props> = async () => {
  const data = {
    title: "N4IT - NestJS solution design. Development. Opensource.",
    description:
      "With N4IT you can focus on your idea without worrying about software. We enable you to go from concept to go-live.",
    url: "https://n4it.nl",
  };

  return {
    props: { data },
  };
};

export default Home;
