import React, { useContext } from "react";
import { useRouter } from "next/navigation";
import {
  List,
  Divider,
  ListItem,
  ListItemText,
  SwipeableDrawer,
} from "@mui/material";
import { Context } from "../../store/store";

import useStyles from "./drawer.styles";

function AppDrawer() {
  const [state, dispatch] = useContext<any>(Context);

  const updateDrawer = () => {
    dispatch({
      type: "TOGGLE_DRAWER",
    });
  };

  const classes = useStyles();

  const router = useRouter();

  const sideList = (
    <div className={classes.list}>
      <Divider />
      <List>
        <ListItem
          button
          className={classes.drawerLink}
          onClick={() => router.push("/introduction-meeting")}
        >
          <ListItemText
            primary={"Introduction"}
            className={classes.drawerText}
          />
        </ListItem>
      </List>
    </div>
  );

  return (
    <div>
      <SwipeableDrawer
        open={state.drawer}
        onClose={updateDrawer}
        onOpen={updateDrawer}
      >
        <div
          tabIndex={0}
          role="button"
          onClick={updateDrawer}
          onKeyDown={updateDrawer}
        >
          {sideList}
        </div>
      </SwipeableDrawer>
    </div>
  );
}

export default AppDrawer;
