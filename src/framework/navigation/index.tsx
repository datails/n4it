import React, { useState, useEffect, useContext } from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import classNames from "classnames";
import { Context } from "../../store/store";
import useStyles from './navigation.styles';
import { AppButton } from "@/components";
import Link from "next/link";

export default function AppNavigation() {
  const classes = useStyles();
  // @ts-ignore
  const [, dispatch] = useContext(Context);
  const [atTopOfPage, setAtTopOfPage] = useState(true);
  const [sleep, setSleep] = useState(false);

  const handleScroll = () => {
    setAtTopOfPage(window.pageYOffset < 275 ? true : false);
    setSleep(window.pageYOffset < 350 ? true : false);
  };

  // show or hide the drawer
  const toggleDrawer = () => {
    dispatch({
      type: "TOGGLE_DRAWER",
    });
  };

  // component did unmount
  useEffect(() => {
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  // component did mount
  useEffect(() => window.addEventListener("scroll", handleScroll), []);

  return (
    <div className={classes.root}>
      <AppBar
        color="default"
        position="relative"
        className={
          atTopOfPage
            ? classes.navigationBarInitial
            : sleep
              ? classNames(classes.navigationBar, classes.navigationBarHide)
              : classes.navigationBar
        }
      >
        <Toolbar className={classes.navigationBarInner}>
          <div className={classes.flex}>
              <Typography variant="h4" className={classes.title}>
                N4IT
              </Typography>
          </div>
        </Toolbar>
      </AppBar>
      <div className={atTopOfPage ? classes.offset0 : classes.offset}></div>
    </div>
  );
}
