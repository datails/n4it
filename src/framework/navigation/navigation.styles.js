import makeStyles from "@mui/styles/makeStyles";
import createStyles from "@mui/styles/createStyles";

export default makeStyles((theme) =>
createStyles({
  heartBeatIcon: {
    color: "#FFF",
    fontSize: "2rem",
  },
  title: {
    color: "#FFF",

  },
  heartBeatIconAwake: {
    color: "#000",
  },
  menuButton: {
    color: "#FFF",
    marginRight: "30px",
  },
  navigationBar: {
    position: "fixed",
    zIndex: 1000,
    top: "-74px",
    transition: "transform 0.2s",
    transform: "translateY(74px)",
    backgroundColor: "#151515",
  },
  navigationBarHide: {
    transform: "translateY(0)",
  },
  navigationBarInitial: {
    backgroundColor: "#151515",
    boxShadow: "none",
  },
  navigationBarInner: {
    maxWidth: "1200px",
    width: "100%",
    margin: "0 auto",
    justifyContent: "center",
    display: "flex",
  },
  offset: {
    height: "74px",
  },
  offset0: {
    height: 0,
  },
  root: {
    flexGrow: 1,
    maxHeight: "64px",
  },
  titleAwake: {
    color: "#000",
  },
  logo: {
    display: "flex",
    justifySelf: "center",
    alignSelf: "center",
    backgroundSize: "cover",
  },
  logoRevert: {
    display: "flex",
    justifySelf: "center",
    alignSelf: "center",
    backgroundSize: "cover",
  },
  flex: {
    display: "flex",
    justifyContent: "center",
  },
})
);