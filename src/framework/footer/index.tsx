import React from "react";
import { Grid, Link, Typography } from "@mui/material";
//@ts-ignore
import classNames from "classnames";
import useStyles from "./footer.styles";

function Footer() {
  const classes = useStyles();

  return (
    <footer>
      <Grid
        container
        className={classes.container}
        justifyContent="space-around"
      >
        <Grid item xs={12} sm={12} md={4} lg={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.text}>
            With N4IT you can focus on your idea without worrying about software. We enable you to go from concept to go-live.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4} className={classes.content}>

          <Typography variant="body1" className={classes.anchor}>
            <Link
              href="mailto:hello@n4it.nl"
              target="_blank"
              color="inherit"
              underline="hover"
            >
              hello@n4it.nl
            </Link>
          </Typography>
        </Grid>
      </Grid>
    </footer>
  );
}

export default Footer;
