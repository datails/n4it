import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export default makeStyles((theme) =>
  createStyles({
    anchor: {
      display: "flex",
      flexDirection: "column",
      fontSize: "1rem",
      lineHeight: 2,
      fontWeight: 300,
      color: "#FFF",
      fontSize: "1.5rem!important",
    },
    container: {
      background: "#151515",
      margin: "0",
      width: "100%",
      padding: "40px",
      [theme.breakpoints?.down("md")]: {
        padding: "40px 0",
      },
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      padding: "40px",
    },
    image: {
      margin: "35px 0",
      width: "100%",
    },
    margin: {
      margin: theme.spacing(1),
    },
    title: {
      fontSize: "1rem !important",
      fontWeight: 500,
      marginBottom: "1.5rem",
      color: "#FFF",
    },
    text: {
      fontSize: "1.2rem!important",
      lineHeight: 1.2,
      fontWeight: 300,
      textAlign: "justify",
      color: "#FFF",
    },
    backgroundMain: {
      padding: "0",
      textAlign: "center",
      background: "#2B2E31",
    },
    colorLight: {
      color: "#FFF",
      fontWeight: 700,
    },
  })
);
